TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    ustring.cpp \
    stringtest.cpp

HEADERS += \
    ustring.h \
    stringtest.h
