#include "ustring.h"


uString::uString( )
{
    length = 0;
    capacity = 1;
    string = new char[ capacity ];
    string[ length ] = '\0';
}

uString::uString( const uString& str )
{
    if( str.string == nullptr )
    {
        length = 0;
        capacity = 1;
        string = new char [ capacity ];
        string[ length ] = '\0';
    }
    else
    {
        length = str.length;
        capacity = length + 1;
        string = new char [ capacity ];
        memmove( string, str.string, sizeof( char ) * length );
        string[ length ] = '\0';
    }
}

uString::uString( uString&& str )
{
    using std::swap;
    swap( string, str.string );
    swap( length, str.length );
    swap( capacity, str.capacity );
}

uString::uString( const char* input_string )
{
    if( input_string == nullptr )
    {
        length = 0;
        capacity = 1;
        string = new char[ capacity ];
        string[ length ] = '\0';
    }
    else
    {
        length = strlen( input_string );
        capacity = length + 1;
        string = new char[ capacity ];
        memmove( string, input_string, sizeof( char ) * length );
        string[ length ] = '\0';
    }
}

void uString::reserve ( size_t capacity )
{

    if( this->capacity >= capacity )
	{
        return;
	}
    else
    {
        char *temp_string = new char[ capacity ];
        memmove( temp_string, string, length );
        this->capacity = capacity;
        delete [] string;
        string = temp_string;
        string[ length ] = '\0';
    }
}

void uString::push_back( const char s )
{
    if( string == NULL )
    {
        capacity = 2;
        length = 1;
        string[ length ] = s;
        string[ ++length ] = '\0';
    }
    else
    { 
        if( length + 1 >= capacity )
            reserve( capacity * 2 );
        string[ length ] = s;
        string[ ++length ] = '\0';
    }
}

void uString::pop_back( )
{
	if( length == 0 )
		return;
	if( string == nullptr )
		return;
	string[ --length ] = '\0';
}

void uString::clear( )
{
    delete [] string;
	capacity = length = 0;
}

const uString &uString::operator= ( const uString& input_str )
{ 
    if( this != &input_str )
    {
        reserve( input_str.length + 1 );
        length = input_str.length;
        memmove( this->string, input_str.string, input_str.length );
        string[ length ] = '\0';
    }
    return *this;
}


const uString &uString::operator= ( const char *input_str )
{
    uString temp( input_str );
    *this = temp;
    return *this;
}

char uString::operator[] ( size_t i )
{
    return string[ i ];
}


bool uString::operator== ( const uString& input_str ) const
{
    int result = std::strcmp(this->string, input_str.string);
    return result == 0;
}


bool uString::operator< ( const uString& input_str ) const
{
    int result = std::strcmp( this->string, input_str.string );
    return result < 0;
}


bool uString::operator> ( const uString& input_str ) const
{
    int result = std::strcmp( this->string, input_str.string );
    return result > 0;
}

const uString operator+ ( const uString& str1, const uString& str2 )
{
    uString string;

    string.capacity = string.length = str1.length + str2.length;
    string.string = new char[ string.length ];
    memmove( string.string, str1.string, str1.length * sizeof ( char ) );
    memmove( string.string + str1.length, str2.string, str2.length * sizeof( char ) );
    return string;
}

const uString operator + ( const uString& str1, const char* str2 )
{
    return str1 + uString( str2 );
}

const uString operator + ( const char* str1, const uString& str2 )
{
    return uString( str1 ) + str2;
}

const uString& uString::operator += ( const uString& str )
{
    *this = *this + str;
    return *this;
}

const uString& uString::operator += ( const char* str )
{
    *this = *this + str;
    return *this;
}

uString::~uString()
{
	length = capacity = 0;
    delete [] string;
}
