#include "stringtest.h"

using namespace std;

void Test_0()
{
    cout << "Targem test" << endl;
    vector<uString> vectorstring;
    uString s;
    cout << "Enter \"exit\" for end the enter words." << endl;
    for( ;; ) {
        cout << "Enter word: ";
        cin >> s;
        if( s == "exit" )
            break;
        vectorstring.push_back(s.Str());
    }
    sort(vectorstring.rbegin(), vectorstring.rend());
    cout << "Result of sort: " << endl;
    for(size_t i = 0; i < vectorstring.size(); ++i)
        cout << " - " << vectorstring.at(i) << endl;
    cout << endl;
}

void Test_1()
{
    cout << "Test 1 - cout & cin test" << endl;
    uString string;
    cout << "Enter string: ";
    cin >> string;
    cout << "Your string: " << string << endl << endl;
}

void Test_2()
{
    cout << "Test 2 - push_back test" << endl;
    uString string;
    cout << "Enter string: ";
    cin >> string;
    cout << string << endl;
    char s = '\0';
    for(size_t i = 0; i <= 2; ++i)
    {
        cout << "Do " << i << ". Enter one char symbol: " << endl;
        cin >> s;
        string.push_back(s);
        cout << "push back result: " << string << endl;
        cout << "string lenght: " << string.Lenght() << endl;
    }
    cout << endl;
}

void Test_3()
{
    cout << "Test 3 - pop back test" << endl;
    uString string;
    cout << "Enter string: ";
    cin >> string;
    cout << string << endl;
    for( size_t i = 0; i <= 2; ++i)
    {
        cout << "Do " << i << endl;
        string.pop_back();
        cout << "pop back result: " << string << endl;
        cout << "string lenght: " << string.Lenght() << endl;
    }
    cout << endl;
}

void Test_4()
{
    cout << "Test 4 - addition test" << endl;
    uString str1, str2, str3;
    cout << "str3 = str1 + str2" << endl;
    cout << "Enter str1: ";
    cin >> str1;
    cout << "Enter str2: ";
    cin >> str2;
    str3 = str1 + str2;
    cout << "str3 = " << str3 << endl << endl;

    cout << "str3 = str1 + \"char* s2\"" << endl;
    cout << "Enter str1: ";
    cin >> str1;
    cout << "Enter s2: ";
    char* s2 = new char[100];
    cin >> s2;
    str3 = str1 + s2;
    cout << "str3 = " << str3 << endl << endl;

    cout << "str3 = \"char* s1\" + str2" << endl;
    cout << "Enter s1: ";
    char* s1 = new char[100];
    cin >> s1;
    cout << "Enter str2: ";
    cin >> str2;
    str3 = s1 + str2;
    cout << "str3 = " << str3 << endl << endl;
    delete [] s1;
    delete [] s2;
}

void Test_5()
{
    cout << "Test 5 - addition assignment test" << endl;
    uString str1, str2;
    cout << "str1 += str2" << endl;
    cout << "Enter str1: ";
    cin >> str1;
    cout << "Enter str2: ";
    cin >> str2;
    str1 += str2;
    cout << "str1 = " << str1 << endl << endl;

    cout << "str1 += \"char* s2\"" << endl;
    cout << "Enter str1: ";
    cin >> str1;
    char *s2 = new char[100];
    cout << "Enter s2: ";
    cin >> s2;
    //str1 += s2;
    cout << "str1 = " << str1 << endl << endl;
    delete [] s2;
}

void Test_6()
{
    cout << "Test 6 - clear test" << endl;
    uString string;
    cout << "Enter string: ";
    cin >> string;
    cout << "string: " << string << endl;
    cout << "string lenght: " << string.Lenght() << endl;
    cout << "string.clear()" << endl;
    string.clear();
    cout << "string lenght: " << string.Lenght() << endl;
    cout << string << endl << endl;

}

void stringTest(int numbtest)
{
    cout << endl << "Information about test: ";
    switch (numbtest) {
    case 0:
        // Targem_test();
        Test_0();
        break;
    case 1:
        Test_1();
        break;
    case 2:
        Test_2();
        break;
    case 3:
        Test_3();
        break;
    case 4:
        Test_4();
        break;
    case 5:
        Test_5();
        break;
    case 6:
        Test_6();
        break;
    default:
        break;
    }
}
