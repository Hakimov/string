#include <cstdio>
#include <fstream>
#include <iostream>
#include "ustring.h"
#include "ustring.cpp"
#include "stringtest.h"

using namespace std;


int main()
{
    ifstream fin("file.in");
    ofstream fout("file.out");

    vector< uString > vectorstring;
    uString s;
    while( fin >> s )
        vectorstring.push_back( s );

    sort( vectorstring.rbegin(), vectorstring.rend() );

    for( size_t i = 0; i < vectorstring.size(); ++i )
        fout << vectorstring.at( i ) << endl;


    return 0;
}





