#ifndef USTRING_H
#define USTRING_H

#include <iostream>
#include <cassert>
#include <cstring>

class uString
{
public:
    uString( );
    uString( const uString& );
    uString( uString&& );
    uString( const char* );
    ~uString( );

    void reserve( size_t capacity );
    void push_back( const char s );
    void pop_back( );
    void clear( );
    size_t Lenght( ) { return length; }
    size_t Capacity( ) { return capacity; }
    const char *Str( ) { return string; }

    const uString &operator = ( const uString& input_str );
    const uString &operator = ( const char* input_str );
    char operator [] ( size_t i );
    bool operator == ( const uString& ) const;
    bool operator < ( const uString& ) const;
    bool operator > ( const uString& ) const;
    const uString& operator += ( const uString& );
    const uString& operator += ( const char* );
    friend const uString operator + ( const uString&, const uString& );
    friend std::ostream &operator << ( std::ostream& out, uString& s )
    {        
        std::copy( s.string, s.string + s.length, std::ostreambuf_iterator< char >( out ) );
        return out;
    }
    friend std::istream &operator >> ( std::istream& in, uString& s )
    {
        s.length = 0;
        while ( in.peek() && isspace( in.peek() ) )
        {
            in.get();
        }
        while (in.peek() && isgraph( in.peek() ) )
        {
            s.push_back( in.get() );
        }
        return in;
    }

private:
    char *string = nullptr;
    size_t length = 0;
    size_t capacity = 0;
};


#endif // USTRING_H
